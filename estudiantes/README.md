# Primer actividad

En este directorio, los estudiantes deberán crear un archivo de texto plano
respetando la siguientes características:

## Nombre del archivo

* El nombre debe estar en [snake case](http://en.wikipedia.org/wiki/Snake_case)
* No debe tener extensión
* El nombre será el usuario de GitLab

## Contenido del archivo

* Archivo separado por *coma (,)* [Archivo CSV](https://es.wikipedia.org/wiki/Valores_separados_por_comas)
* Campos en el archivo:
  * Nombre
  * Apellido
  * Fecha de Nacimiento, Formato AÑO-MES-DIA EJ: 2006-10-15
  * DNI

